"""story4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include
from django.urls import re_path
from mysite.views import index as index
import mysite.urls as mysite

"""
from django.urls import include, path
from django.urls import re_path, include
from django.conf.urls import url, include
from mysite.views import homepage as home
from mysite.views import profile as profile
from mysite.views import experiences as experiences
from mysite.views import skills as skills
from mysite.views import achievements as achievements
from mysite.views import contacts as contacts
from mysite.views import form as form
"""

urlpatterns = [
    #path('mysite/', include('mysite.urls')),
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^mysite/', include('mysite.urls')),
    re_path(r'^', include(mysite)),
]
"""
    re_path(r'^homepage.html', home, name="home"),
    re_path(r'^profile.html', profile, name="profile"),
    re_path(r'^experiences.html', experiences, name="experiences"),
    re_path(r'^skills.html', skills, name="skills"),
    re_path(r'^achievements.html', achievements, name="achievements"),
    re_path(r'^contacts.html', contacts, name="contacts"),
    re_path(r'^form.html', form, name="form"),
"""
