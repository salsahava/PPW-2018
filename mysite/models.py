from django.db import models

class Schedule(models.Model):
    activity = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    day = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    category = models.CharField(max_length=100)

    def __str__(self):
        return self.activity
