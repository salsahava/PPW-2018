from django.shortcuts import render
from .forms import Schedule_Form
from .models import Schedule
from django.http import HttpResponseRedirect

response = {'author' : 'Salsabila Hava Qabita'}

def index(request):
    return render(request, "homepage.html")

def profile(request):
    return render(request, "profile.html")

def experiences(request):
    return render(request, "experiences.html")

def skills(request):
    return render(request, "skills.html")

def achievements(request):
    return render(request, "achievements.html")

def contacts(request):
    return render(request, "contacts.html")

def form(request):
    return render(request, "form.html")

def addschedule(request):
    form = Schedule_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['activity'] = request.POST['activity']
        response['place'] = request.POST['place']
        response['day'] = request.POST['day']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['category'] = request.POST['category']

        schedule = Schedule(activity=response['activity'], place=response['place'],
                            day=response['day'], date=response['date'],
                            time=response['time'], category=response['category'])

        schedule.save()
    else:
        print('The input is invalid')
        
    response['schedule_form'] = form
    html = 'addschedule.html'
    return render(request, html, response)

def schedule(request):
    if (request.method == 'POST'):
        Schedule.objects.all().delete()
        
    schedule = Schedule.objects.all()
    response['schedule'] = schedule
    html = 'schedule.html'
    return render(request, html, response)











        
