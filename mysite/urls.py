#from django.urls import path
#from . import views

from django.conf.urls import url
from .views import index
from .views import profile
from .views import experiences
from .views import skills
from .views import achievements
from .views import contacts
from .views import form
from .views import schedule
from .views import addschedule

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^profile/', profile, name='profile'),
    url(r'^experiences/', experiences, name='experiences'),
    url(r'^skills/', skills, name='skills'),
    url(r'^achievements/', achievements, name='achievements'),
    url(r'^contacts/', contacts, name='contacts'),
    url(r'^form/', form, name='form'),
    url(r'^schedule/', schedule, name='schedule'),
    url(r'^addschedule/', addschedule, name='addschedule'),
]
