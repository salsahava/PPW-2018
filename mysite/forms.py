from django import forms

class Schedule_Form(forms.Form):
    error_messages = {
        'required': 'This field is required.',
        'invalid': 'Please fill this field with the right type of input',
        }

    attrs = {
        'class': 'form-control'
        }

    activity = forms.CharField(label='Activity', required=True, widget=forms.TextInput(attrs=attrs))
    place = forms.CharField(label='Place', required=True, widget=forms.TextInput(attrs=attrs))
    day = forms.CharField(label='Day', required=True, widget=forms.TextInput(attrs=attrs))
    date = forms.DateField(label='Date', required=True, widget=forms.DateInput(attrs=attrs))
    time = forms.TimeField(label='Time', required=True, widget=forms.TimeInput(attrs=attrs))
    category = forms.CharField(label='Category', required=True, widget=forms.TextInput(attrs=attrs))
